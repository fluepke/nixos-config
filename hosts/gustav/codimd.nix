{ ... }:

{
  users.users.nginx.extraGroups = [ "codimd" ];

  services.nginx.virtualHosts."pad.fluep.ke" = {
    enableACME = true;
    forceSSL = true;
    locations."/".proxyPass = "http://unix:/run/codimd/codimd.sock";
  };

  services.codimd = {
    enable = true;
    configuration = {
      path = "/run/codimd/codimd.sock";
      domain = "pad.fluep.ke";
      protocolUseSSL = true;
      useCDN = false;
      allowGravatar = false;
      allowFreeURL = true;
      db = {
        dialect = "postgres";
        host = "/run/postgresql";
      };
    };
  };

  systemd.services.codimd = {
    serviceConfig = {
      UMask = "0007";
      RuntimeDirectory = "codimd";
    };
  };

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "codimd" ];
    ensureUsers = [
      {
        name = "codimd";
        ensurePermissions."DATABASE codimd" = "ALL PRIVILEGES";
      }
    ];
  };
}
