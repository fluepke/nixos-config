{ sources, pkgs, lib, config, ... }:

{
  imports = [
    (fetchTarball "https://gitlab.com/simple-nixos-mailserver/nixos-mailserver/-/archive/master.tar.gz")
  ];

  mailserver = {
    enable = true;
    fqdn = "gustav.fluep.ke";
    messageSizeLimit = 52428800;
    dkimKeyBits = 2048;

    domains = [ "fluep.ke" ];
    loginAccounts = {
      "me@fluep.ke" = {
        hashedPasswordFile = "/var/src/secrets/mail/fluepke";
        aliases = [
          "@fluep.ke"
        ];

        catchAll = [
          "fluep.ke"
        ];
      };
      "alertmanager@fluep.ke" = {
        hashedPasswordFile = "/var/src/secrets/alertmanager/mail";
        sendOnly = true;
      };
    };

    # Use Let's Encrypt certificates. Note that this needs to set up a stripped
    # down nginx and opens port 80.
    certificateScheme = 3;

    # Enable IMAP and POP3
    enableImap = true;
    enablePop3 = false;
    enableImapSsl = true;
    #enablePop3Ssl = true;

    # we already have bind
    localDnsResolver = false;

    # Enable the ManageSieve protocol
    enableManageSieve = true;

    # whether to scan inbound emails for viruses (note that this requires at least
    # 1 Gb RAM for the server. Without virus scanning 256 MB RAM should be plenty)
    virusScanning = false;
  };
}
