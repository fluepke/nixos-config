{ config, pkgs, ... }:

{
  services.nginx.virtualHosts."fluep.ke" = {
    enableACME = true;
    forceSSL = true;
    locations."/".root = ./website;
  };
}

