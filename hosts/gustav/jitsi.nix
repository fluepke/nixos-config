{ pkgs, lib, ... }:

let
  nur-no-pkgs = import (builtins.fetchTarball ("https://github.com/nix-community/NUR/archive/master.tar.gz")) { inherit pkgs; };
in {
  imports = [
    nur-no-pkgs.repos.kolloch.modules.jitsi
    ./turn.nix
    ./prosody.nix
  ];

  nixpkgs.config.packageOverrides = pkgs: {
    inherit (nur-no-pkgs.repos.kolloch) jitsi-meet jitsi-videobridge jicofo;
  };

  services.jitsi-videobridge.openFirewall = true;
  services.jitsi-meet = {
    enable = true;
    hostName = "meet.fluep.ke";
    config = {
      useIPv6 = true;
      useStunTurn = true;
      startAudioOnly = true;
      desktopSharingFirefoxDisabled = true;
      desktopSharingFrameRate.min = 5;
      desktopSharingFrameRate.max = 10;
      disableThirdPartyRequests = true;
      p2p = {
        useStunTurn = true;
        stunServers = [
          { urls = "stun:turn.fluep.ke:3478"; }
          { urls = "stun:turn.fluep.ke:3479"; }
        ];
      };
    };
  };

  services.nginx.virtualHosts."meet.fluep.ke" = {
    enableACME = true;
    forceSSL   = true;
  };
}
