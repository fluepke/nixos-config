{ ... }:

{
  networking.wireguard.interfaces.wg-fluepke-home = {
    privateKeyFile = "/home/fluepke/wg-fluepke-home.key";
    generatePrivateKeyFile = true;
    listenPort = 51820;
    ips = [ "45.158.41.1/32" "2a0f:5381::1/128" ];
    allowedIPsAsRoutes = true; # default
    peers = [
      {
        allowedIPs = [ "2a0f:5380::/64" "45.158.40.0/24" ];
        publicKey = "dTix1LEIWe22eEH67rASEOigE/WT4E3BkjM6VcQhIgw=";
      }
    ];
  };
}
