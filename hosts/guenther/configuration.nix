{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./wireguard.nix
      ./router
      ../../common
      ../../dns
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "guenther"; # Define your hostname.
  networking.domain = "fluep.ke";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp1s0 = {
    ipv6.addresses = [{ address = "2a01:581:1:9::3"; prefixLength = 127; }];
    ipv4.addresses = [{ address = "62.176.250.86"; prefixLength = 30; }];
  };
  networking.interfaces.enp7s0 = {
    ipv6.addresses = [{ address = "2a0f:5381::1"; prefixLength = 64; }];
    ipv4.addresses = [{ address = "45.158.41.1"; prefixLength = 24; }];
  };
  networking.defaultGateway6 = { address = "2a01:581:1:9::2"; interface = "enp1s0"; };
  networking.defaultGateway = { address = "62.176.250.85"; interface = "enp1s0"; };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

}

