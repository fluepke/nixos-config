{ pkgs, ... }:

let
  bird-lgproxy-go = pkgs.buildGoPackage {
    pname = "bird-lgproxy-go";
    version = "unstable-20200518";
    goPackagePath = "proxy";

    src = fetchGit {
      url = "https://github.com/xddxdd/bird-lg-go";
      rev = "4df9006c8118bf977c27e5e4e978c686b0a809b3";
    };
  };
in {
  systemd.services.bird-lgproxy-go = {
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      User = "bird-lgproxy";
      Group = "bird-lgproxy";
      ExecStart = "${bird-lgproxy-go}/bin/proxy";

      Restart = "always";
      RestartSec = 10;
      ProtectSystem = "strict";
      NoNewPrivileges = true;
      ProtectControlGroups = true;
      PrivateTmp = true;
      PrivateDevices = true;
      DevicePolicy = "closed";
      MemoryDenyWriteExecute = true;
      ProtectHome = true;
    };
    environment = {
      BIRD_SOCKET = "/run/bird.ctl";
      BIRD6_SOCKET = "/run/bird.ctl";
      BIRDLG_LISTEN = ":5000";
    };
  };

  users.users.bird-lgproxy = {
    isSystemUser = true;
    group = "bird-lgproxy";
    extraGroups = [ "bird2" ];
  };
  users.groups.bird-lgproxy = {};

  networking.firewall.extraCommands = ''
    ip6tables -I INPUT --proto tcp --source 2a0f:4ac0::10 --dport 5000 -j ACCEPT
  '';
}
