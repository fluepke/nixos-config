{ lib, config, pkgs, ... }:

{
  imports = [
      ./lg.nix
    ];

  boot.kernel.sysctl."net.ipv6.conf.enp1s0.accept_ra" = false;

  networking.firewall.allowedTCPPorts = [ 179 ];
  networking.firewall.allowedUDPPorts = [ config.networking.wireguard.interfaces.wg-fluepke-home.listenPort ];
  services.bird2.enable = true;
  services.bird2.config = lib.fileContents ./bird.conf;

  services.dhcpd4 = {
    enable = true;
    interfaces = [ "enp7s0" ];
    extraConfig = ''
      option domain-name "luepke.email";
      option domain-name-servers 1.1.1.1, 1.0.0.1;
      subnet 45.158.41.0 netmask 255.255.255.0 {
        range 45.158.41.100 45.158.41.200;
        option routers 45.158.41.1;
        interface enp7s0;
      }
    '';
  };

  services.radvd = {
    enable = true;
    config = ''
      interface enp7s0 {
        AdvSendAdvert on;
        prefix 2a0f:5381::/64 {
          AdvRouterAddr on;
        };
        RDNSS 2606:4700:4700::1111 2606:4700:4700::1001 {};
      };
    '';
  };

  networking.localCommands = ''
    set -x

    ip -6 rule flush || true
    ip -4 rule flush || true
    ip -6 rule add lookup main prio 32000 || true
    ip -4 rule add lookup main prio 32000 || true

    # internal (own net -> own net)
    ip -6 rule add from 2a0f:5380::/29 lookup main suppress_prefixlength 0 prio 1500 || true
    ip -4 rule add from 45.158.40.0/22 lookup main suppress_prefixlength 0 prio 1500 || true

    # outgoing (own net -> internet)
    ip -6 rule add from 2a0f:5380::/29 lookup 208135 prio 2000 || true
    ip -4 rule add from 45.158.40.0/22 lookup 208135 prio 2000 || true

    ip -6 rule add from 2a0f:5380::/29 unreachable prio 3000 || true
    ip -4 rule add from 45.158.40.0/22 unreachable prio 3000 || true
  '';

  networking.firewall.checkReversePath = false;
  boot.kernel.sysctl."net.ipv6.conf.all.forwarding" = true;
  boot.kernel.sysctl."net.ipv4.conf.all.forwarding" = true;

  services.iperf3.enable = true;
  services.iperf3.openFirewall = true;
}
