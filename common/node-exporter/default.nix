{ config, ... }:

let 
  listenAddress = "127.0.0.1";
  nodeExporterPort = toString config.services.prometheus.exporters.node.port;
  nginxExporterPort = toString config.services.prometheus.exporters.nginx.port;
  allowedHosts = ''
    allow ::1/128;
    allow 2a0f:5381::/29;
    allow 127.0.0.1/32;
    allow 45.185.40.0/22;
    deny all;
  '';
in
{
  services.prometheus.exporters.node = {
    enable = true;
    inherit listenAddress;
  };

  services.prometheus.exporters.nginx = {
    enable = true;
    inherit listenAddress;
  };

  services.nginx.statusPage = true;
  services.nginx.virtualHosts."${config.networking.hostName}.${config.networking.domain}" = {
    locations."/node-exporter/metrics".proxyPass = "http://${listenAddress}:${nodeExporterPort}/metrics";
    locations."/node-exporter/metrics".extraConfig = allowedHosts;
    locations."/nginx-exporter/metrics".proxyPass = "http://${listenAddress}:${nginxExporterPort}/metrics";
    locations."/nginx-exporter/metrics".extraConfig = allowedHosts;
  };
}
