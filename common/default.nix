{ lib, config, pkgs, ... }:

{
  imports = [
    ("${fetchTarball "https://github.com/rycee/home-manager/archive/master.tar.gz"}/nixos")
    ./nginx
    ./node-exporter
  ];

  networking.nameservers = [
    "2606:4700:4700::1111"
    "2606:4700:4700::1001"
    "1.1.1.1"
    "1.0.0.1"
  ];

  boot.kernelPackages = pkgs.linuxPackages_latest;

  time.timeZone = "Etc/UTC";

  environment.systemPackages = with pkgs; [
     wget vim dnsutils termite.terminfo tmux nload
     socat curl tree nmap git iperf bat jq
  ];

  programs.mtr.enable = true;

  services.openssh = {
    enable = true;
    ports = [ 62954 ];
    passwordAuthentication = false;
    challengeResponseAuthentication = false;
    permitRootLogin = "no";
  };

  security.sudo.wheelNeedsPassword = false;
  users.users.fluepke = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    shell = pkgs.zsh;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIFPW/LUTmJn5Ip8x5HjrjENjCh+u9aA60uGzLpNsBag cardno:000611180084"
    ];
  };

  security.acme.email = "security@luepke.email";
  security.acme.acceptTerms = true;

  home-manager.useGlobalPkgs = true;
  home-manager.users.fluepke = {
    programs.git = {
      enable = true;
      userName = "@fluepke";
      userEmail = "git@luepke.email";
    };
  };
}
