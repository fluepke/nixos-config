#!/usr/bin/env bash

NIX_SSHOPTS="-p 62954" nixos-rebuild switch --target-host guenther.fluep.ke -I nixos-config=./hosts/guenther/configuration.nix --use-remote-sudo
