{ config, ... }:

let
  master = true;
  masters = [];
  slaves = ["any"];
  listenAddress = "127.0.0.1";
  port = 9119;
in {
  networking.firewall.allowedTCPPorts = [ 53 ];
  networking.firewall.allowedUDPPorts = [ 53 ];

  # the bind module assumes that bind will be used
  # as a local dns cache, but this will not work here
  networking.resolvconf.useLocalResolver = false;

  services.bind = {
    enable = true;
    forwarders = [];
    cacheNetworks = [];
    extraConfig = ''
      statistics-channels {
        inet 127.0.0.1 port 8053 allow { 127.0.0.1; };
      };
    '';
    zones = [
      {
        name = "fluep.ke";
        file = ./fluep.ke.zone;
        inherit master masters slaves;
      }
      {
        name = "1.8.3.5.f.0.a.2.ip6.arpa";
        file = ./1.8.3.5.f.0.a.2.ip6.arpa.zone;
        inherit master masters slaves;
      }
      {
        name = "40.158.45.in-addr.arpa";
        file = ./40.158.45.in-addr.arpa.zone;
        inherit master masters slaves;
      }
      {
        name = "41.158.45.in-addr.arpa";
        file = ./41.158.45.in-addr.arpa.zone;
        inherit master masters slaves;
      }
      {
        name = "42.158.45.in-addr.arpa";
        file = ./42.158.45.in-addr.arpa.zone;
        inherit master masters slaves;
      }
      {
        name = "43.158.45.in-addr.arpa";
        file = ./43.158.45.in-addr.arpa.zone;
        inherit master masters slaves;
      }
    ];
  };

  services.prometheus.exporters.bind = {
    enable = true;
    inherit listenAddress port;
  };
  services.nginx.virtualHosts."${config.networking.hostName}.${config.networking.domain}" = {
    locations."/bind-exporter/metrics".proxyPass = "http://${listenAddress}:${toString port}/metrics";
    locations."/bind-exporter/metrics".extraConfig = config.services.nginx.virtualHosts."${config.networking.hostName}.${config.networking.domain}".locations."/node-exporter/metrics".extraConfig;
  };
}

